import { BrowserModule } from '@angular/platform-browser';
import { NgModule, DEFAULT_CURRENCY_CODE, LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import localePt from '@angular/common/locales/pt';
import localePtExtra from '@angular/common/locales/extra/pt';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PerfilComponent } from './pages/perfil/perfil.component';
import { LoginComponent } from './pages/login/login.component';
import { SobreComponent } from './pages/sobre/sobre.component';
import { PaginaNaoEncontradaComponent } from './pages/pagina-nao-encontrada/pagina-nao-encontrada.component';
import { InicioComponent } from './pages/inicio/inicio.component';
import { HomeInicioComponent } from './pages/home-inicio/home-inicio.component';
import { PropostaDeTCCComponent } from './pages/proposta-de-tcc/proposta-de-tcc.component';
import { PropostasDeTCCComponent } from './pages/propostas-de-tcc/propostas-de-tcc.component';
import { ListaDePublicacoesComponent } from './pages/propostas-de-tcc/lista-de-publicacoes/lista-de-publicacoes.component';
import { CadastroTccComponent } from './pages/propostas-de-tcc/cadastro-tcc/cadastro-tcc.component';
import { AlterarPropostaTccComponent } from './pages/propostas-de-tcc/alterar-proposta-tcc/alterar-proposta-tcc.component';
import { PropostaDeEstagioFormComponent } from './pages/proposta-de-estagio/proposta-de-estagio-form/proposta-de-estagio-form.component';
import { PropostaDeEstagioListaComponent } from './pages/proposta-de-estagio/proposta-de-estagio-lista/proposta-de-estagio-lista.component';
import { PropostaDeTccFormComponent } from './pages/proposta-tcc/proposta-de-tcc-form/proposta-de-tcc-form.component';
import { PropostaDeTccListaComponent } from './pages/proposta-tcc/proposta-de-tcc-lista/proposta-de-tcc-lista.component';

import { ProviderGenericoProvider } from './providers/provider-generico';

registerLocaleData(localePt, 'pt', localePtExtra);


@NgModule({
  declarations: [
    AppComponent,
    PerfilComponent,
    LoginComponent,

    SobreComponent,
    PaginaNaoEncontradaComponent,

    InicioComponent,
    HomeInicioComponent,

    PropostaDeTCCComponent,
    PropostasDeTCCComponent,
    CadastroTccComponent,
    AlterarPropostaTccComponent,

    ListaDePublicacoesComponent,

    PropostaDeEstagioFormComponent,
    PropostaDeEstagioListaComponent,
    
    PropostaDeTccFormComponent,
    PropostaDeTccListaComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [
    {
      provide: LOCALE_ID, useValue: 'pt'
    },
    {
      provide: DEFAULT_CURRENCY_CODE, useValue: 'BRL'
    },
    ProviderGenericoProvider,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
