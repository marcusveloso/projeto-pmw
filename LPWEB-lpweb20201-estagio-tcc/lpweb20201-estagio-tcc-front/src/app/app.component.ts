import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './providers/services/auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
 
  constructor(public auth$: AuthService, 
    private router: Router) { }


  public autenticado: Boolean = false;
  public toggled: string= "";

  urlPublica: any = ['/login', '/registro'];

  ngOnInit() { this.getLoginSession();}

  init() {
    if(this.autenticado){
      this.router.navigate(['/perfil']);
    } else {
      this.router.navigate(['/login']);
    }
  }

  async getLoginSession() {
    let valor = localStorage.getItem('user');
      this.autenticado = valor != null;
    return valor != null;
  }

  abrir(event: any, nomeServico:any) {
    this.router.navigate([nomeServico]);
  }

  async getIsLogin() {    
    await this.auth$.isLogin().then((session)=>{
      this.autenticado = session;
    });    
  }

  logout() {
    this.autenticado = false;
    this.auth$.logout().then(() => {
      this.autenticado = true;
      this.router.navigate(['/login']);
      location.reload();
    });
  }
  

  toggleCollapsed() {
    if(this.toggled == "toggled"){
      this.toggled ="";
    } else {
    this.toggled = "toggled";
    }
  }
}