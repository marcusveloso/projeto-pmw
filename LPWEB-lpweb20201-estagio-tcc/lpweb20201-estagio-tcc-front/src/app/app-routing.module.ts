import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { PerfilComponent } from './pages/perfil/perfil.component';
import { SobreComponent } from './pages/sobre/sobre.component';
import { PaginaNaoEncontradaComponent } from './pages/pagina-nao-encontrada/pagina-nao-encontrada.component';
import { InicioComponent } from './pages/inicio/inicio.component';
import { HomeInicioComponent } from './pages/home-inicio/home-inicio.component';
// import { PropostasDeTCCComponent } from './pages/propostas-de-tcc/propostas-de-tcc.component';
// import { PropostaDeTCCComponent } from './pages/proposta-de-tcc/proposta-de-tcc.component';
// import { ListaDePublicacoesComponent } from './pages/propostas-de-tcc/lista-de-publicacoes/lista-de-publicacoes.component';
import { CadastroTccComponent } from './pages/propostas-de-tcc/cadastro-tcc/cadastro-tcc.component';
import { AlterarPropostaTccComponent } from './pages/propostas-de-tcc/alterar-proposta-tcc/alterar-proposta-tcc.component';
import { PropostaDeTccListaComponent } from './pages/proposta-tcc/proposta-de-tcc-lista/proposta-de-tcc-lista.component';
import { PropostaDeTccFormComponent } from './pages/proposta-tcc/proposta-de-tcc-form/proposta-de-tcc-form.component';
// import { PublicacoesListaComponent } from './pages/proposta-tcc/publicacoes-lista/publicacoes-lista.component';




const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  { path: 'perfil', component: PerfilComponent },
  { path: 'sobre', component: SobreComponent },
  {
    path: 'inicio', component: InicioComponent, children: [
      { path: 'propostas-de-tcc-lista', component: PropostaDeTccListaComponent },
      { path: 'propostas-de-tcc/novo', component: PropostaDeTccFormComponent },
      { path: 'propostas-de-tcc/:id', component: PropostaDeTccFormComponent },
      // { path: 'propostas-de-tcc/lista-de-publicacoes', component: PublicacoesListaComponent },
      // { path: 'propostas-de-tcc', component: PropostasDeTCCComponent },
      // { path: 'propostas-de-tcc/:id', component: PropostaDeTCCComponent },
      // { path: 'propostas-de-tcc/lista-de-publicacoes', component: ListaDePublicacoesComponent },
      // { path: 'propostas-de-tcc/cadastro-tcc', component: CadastroTccComponent },
      // { path: 'propostas-de-tcc/alterar-proposta-tcc/:id', component: AlterarPropostaTccComponent },
      { path: '', component: HomeInicioComponent }
    ]
  },
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: '**', component: PaginaNaoEncontradaComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
