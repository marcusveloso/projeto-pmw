import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { PerfilService } from '../../providers/services/perfil.service';
import { AuthService } from '../../providers/services/auth/auth.service';
import { UserService } from '../../providers/services/user.service';
import { AppComponent } from 'src/app/app.component';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {
  user: any;
  perfil: any;
  temPerfil = null;

  constructor(public auth$: AuthService,
              private perfil$: PerfilService,
              private router: Router,
              private appComponent: AppComponent) { }

  ngOnInit(): void {
    this.user = this.auth$.user();
    if (this.user) {
      this.perfil$.perfilLogado()
        .subscribe(
          dados => {this.perfil = dados;
            this.appComponent.autenticado = true;
          },
          erro => this.temPerfil = false
        );
    } else {
      this.appComponent.autenticado = false;
      this.router.navigate(['/login']);
    }
  }
}
