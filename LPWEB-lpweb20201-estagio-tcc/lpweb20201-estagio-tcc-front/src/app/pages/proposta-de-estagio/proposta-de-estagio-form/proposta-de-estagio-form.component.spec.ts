import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PropostaDeEstagioFormComponent } from './proposta-de-estagio-form.component';

describe('PropostaDeEstagioFormComponent', () => {
  let component: PropostaDeEstagioFormComponent;
  let fixture: ComponentFixture<PropostaDeEstagioFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PropostaDeEstagioFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PropostaDeEstagioFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
