import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PropostaDeEstagioListaComponent } from './proposta-de-estagio-lista.component';

describe('PropostaDeEstagioListaComponent', () => {
  let component: PropostaDeEstagioListaComponent;
  let fixture: ComponentFixture<PropostaDeEstagioListaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PropostaDeEstagioListaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PropostaDeEstagioListaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
