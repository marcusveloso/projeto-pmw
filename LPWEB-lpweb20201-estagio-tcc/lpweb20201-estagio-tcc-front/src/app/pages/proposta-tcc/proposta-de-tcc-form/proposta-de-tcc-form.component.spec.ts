import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PropostaDeTccFormComponent } from './proposta-de-tcc-form.component';

describe('PropostaDeTccFormComponent', () => {
  let component: PropostaDeTccFormComponent;
  let fixture: ComponentFixture<PropostaDeTccFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PropostaDeTccFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PropostaDeTccFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
