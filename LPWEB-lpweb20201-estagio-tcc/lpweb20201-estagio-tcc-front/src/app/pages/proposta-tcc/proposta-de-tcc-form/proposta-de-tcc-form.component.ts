import { Component, OnInit } from '@angular/core';
import { ProviderGenericoProvider } from 'src/app/providers/provider-generico';
import { ActivatedRoute, Router } from '@angular/router';
import { delay } from 'rxjs/operators';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { AuthService } from 'src/app/providers/services/auth/auth.service';
import { AppComponent } from 'src/app/app.component';

@Component({
  selector: 'app-proposta-de-tcc-form',
  templateUrl: './proposta-de-tcc-form.component.html',
  styleUrls: ['./proposta-de-tcc-form.component.css']
})
export class PropostaDeTccFormComponent implements OnInit {
 
  proposta:any;
  
  cadastroForm: FormGroup;
  orientacaoAtual:string;

    constructor(public auth$: AuthService,
                private providerGenericoProvider: ProviderGenericoProvider,
                private route: ActivatedRoute,
                private formBuilder: FormBuilder,
                private router: Router,
                private appComponent: AppComponent
            
    ) { }

    public infox:string;
    countries = [];
    public orientacaoId:any;
    public funcionarioId:any;
    public funcionarioExternoId:any;
    
    ngOnInit(): void {

      this.auth$.getLoginSession().then(retorno =>{
        if(!retorno){
          this.appComponent.autenticado = false;
          this.router.navigate(['/login']);
        }
      })

    let parametro = this.geturlAction();

    this.formulario();

    if(parametro != "novo"){
    this.route.paramMap.subscribe(
      params => {
        this.providerGenericoProvider
          .getId({nomeServico: 'propostas-de-tcc', id: parametro})
          .pipe(delay(2000))
          .subscribe((proposta:any) => 
            {
              this.proposta = proposta;
              this.cadastroForm.patchValue(proposta);
              this.orientacaoId = proposta.orientacao.id;
              this.funcionarioId = proposta.orientacao.professor.id;
              console.log("proposta : ",proposta);
            
              this.orientacao();
  
        this.providerGenericoProvider
            .get({nomeServico:'funcionarios'})
            .subscribe((data: any) => {
          this.listaFuncionario = data.results;
        })
    
        this.providerGenericoProvider
            .get({nomeServico:'colaboradores-externos'})
            .subscribe((data: any) => {
          this.listaExternos = data.results;
        });
            });
          }
        );
      } else {
        this.orientacao();
  
        this.providerGenericoProvider
            .get({nomeServico:'funcionarios'})
            .subscribe((data: any) => {
          this.listaFuncionario = data.results;
        })
    
        this.providerGenericoProvider
            .get({nomeServico:'colaboradores-externos'})
            .subscribe((data: any) => {
          this.listaExternos = data.results;
        });
      }
  }

 geturlAction(){
    
    let res = null;
    let url = this.router.url;
    
    if(url){
      res = url.split('/')[3];
    }

    return res;
  }

  proposta_tcc: any;
  listaOrientacoes: any = [];
  listaFuncionario: any = [];
  listaExternos: any = [];


  /** Lista equipe de Orientação */
  orientacao() {

    this.providerGenericoProvider
        .get({nomeServico:'orientacoes'})
        .subscribe((dados: any) => {
          this.listaOrientacoes = dados.results.filter(
        (tipo) => tipo.tipo === 'tcc'
      );
      // console.log("listaOrientacoes", this.listaOrientacoes);
    });

  }

  /**Gerando formulario para salvar dados de cadastro */
  formulario() {

    this.cadastroForm = this.formBuilder.group({
      id: [null],
      orientacao_id: [null, [Validators.required]],
      titulo: ['', [Validators.required]],
      conceitos: ['', [Validators.required]],
      resultados_esperados: ['', [Validators.required]],
      objetivo: ['', [Validators.required]],
      tecnologias: ['', [Validators.required]],
      metodologia: ['', [Validators.required]],
      membros_da_banca_funcionario: [null],
      membros_da_banca_externo: [null],
    });

  }

  /** Atualizar os dados do Objeto */
  atualizarDados() {

    const banca = [];
    this.proposta_tcc = Object.assign({}, this.proposta_tcc, this.cadastroForm.value);
    this.proposta_tcc.orientacao_id = parseInt(this.proposta_tcc.orientacao_id);
    this.cadastroForm.controls.membros_da_banca_funcionario.value.map (funcionario => {banca.push({membro_interno_id : funcionario});});
    this.cadastroForm.controls.membros_da_banca_externo.value.map(colaboradorExterno => {banca.push({membro_externo_id : colaboradorExterno});});
    this.proposta_tcc.membros_da_banca = banca;

    delete this.proposta_tcc.membros_da_banca_funcionario;
    delete this.proposta_tcc.membros_da_banca_externo;

  }

  /** Adicionar */
  adicionar() {
    this.atualizarDados();
    console.log("this.proposta_tcc.id:> ", this.proposta_tcc.id);
    
    if(this.proposta_tcc.id != null){
    this.providerGenericoProvider
        .put({nomeServico: 'propostas-de-tcc', dados:this.proposta_tcc, id:this.proposta_tcc.id })
        .subscribe((retorno: any) => {
        this.router.navigate(['/inicio/propostas-de-tcc-lista']);
      },
      (error) => console.log(error)
    );
    } else {
      this.providerGenericoProvider
      .post({nomeServico: 'propostas-de-tcc', dados:this.proposta_tcc})
      .subscribe((retorno: any) => {
      this.router.navigate(['/inicio/propostas-de-tcc-lista']);
    },
    (error) => console.log(error)
  );
    }
  }

  voltarParaLista(){
    this.router.navigate(['/inicio/propostas-de-tcc-lista']);
  }

  onSubmit() {

    if (!this.cadastroForm.dirty || !this.cadastroForm.valid) {
      this.cadastroForm.markAllAsTouched();
      return '';
    }
    this.adicionar();
  }
}
