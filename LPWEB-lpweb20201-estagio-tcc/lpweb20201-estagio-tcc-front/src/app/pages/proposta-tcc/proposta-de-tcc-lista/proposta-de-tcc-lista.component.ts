import { Component, OnInit } from '@angular/core';
import { ProviderGenericoProvider } from 'src/app/providers/provider-generico';
import { Router } from '@angular/router';
import { delay } from 'rxjs/operators';
import { AuthService } from 'src/app/providers/services/auth/auth.service';
import { AppComponent } from 'src/app/app.component';

@Component({
  selector: 'app-proposta-de-tcc-lista',
  templateUrl: './proposta-de-tcc-lista.component.html',
  styleUrls: ['./proposta-de-tcc-lista.component.css']
})
export class PropostaDeTccListaComponent implements OnInit {
  propostas = null;

  constructor(public auth$: AuthService,
              private proposta$: ProviderGenericoProvider,
              private router:Router,
              private appComponent: AppComponent) { }

  ngOnInit(): void {
    this.proposta$.get({nomeServico:'propostas-de-tcc'})
      .pipe(delay(1))
      .subscribe(lista => {this.propostas = lista;
      // console.log("lista:> ", lista);
      });

      this.auth$.getLoginSession().then(retorno =>{
        if(!retorno){
          this.appComponent.autenticado = false;
          this.router.navigate(['/login']);
        }
      })
  }

  abrir(item:any){
    this.router.navigate(['/inicio/propostas-de-tcc/'+ (item == undefined ? 'novo': item.id)]);
  }
}