import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PropostaDeTccListaComponent } from './proposta-de-tcc-lista.component';

describe('PropostaDeTccListaComponent', () => {
  let component: PropostaDeTccListaComponent;
  let fixture: ComponentFixture<PropostaDeTccListaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PropostaDeTccListaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PropostaDeTccListaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
