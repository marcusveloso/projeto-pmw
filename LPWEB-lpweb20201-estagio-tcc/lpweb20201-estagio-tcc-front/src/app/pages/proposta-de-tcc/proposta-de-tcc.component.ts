import { Component, OnInit } from '@angular/core';
import { PropostaDeTCCService } from '../../providers/services/proposta-de-tcc.service';
import { ActivatedRoute, Router } from '@angular/router';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'app-proposta-de-tcc',
  templateUrl: './proposta-de-tcc.component.html',
  styleUrls: ['./proposta-de-tcc.component.css']
})
export class PropostaDeTCCComponent implements OnInit {
  proposta = null;

  constructor(private proposta$: PropostaDeTCCService,
    private route: ActivatedRoute,
    private router:Router) { }

  ngOnInit(): void {
    let param = this.geturlAction();

    if(param.toString() != "cadastro-tcc"){
    this.route.paramMap.subscribe(
      params => {
        this.proposta$.get(params.get('id'))
          .pipe(delay(1000))
          .subscribe((proposta) => 
            {
              this.proposta = proposta
              console.log("proposta: ",proposta);
            });
          }
        );
      }
  }

  async geturlAction(){
    let res = null;
    let url = await this.router.url;
    console.log(url);
    
    if(url){
      res = url.split('/')[2];
    }
    return res;
  }

}
