import { Component, OnInit } from '@angular/core';
import { PropostaDeTCCService } from '../../providers/services/proposta-de-tcc.service';
import { delay } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-propostas-de-tcc',
  templateUrl: './propostas-de-tcc.component.html',
  styleUrls: ['./propostas-de-tcc.component.css']
})
export class PropostasDeTCCComponent implements OnInit {
  propostas = null;

  constructor(private proposta$: PropostaDeTCCService,
              private router:Router) { }

  ngOnInit(): void {
    this.proposta$.lista_publicados()
      .pipe(delay(1))
      .subscribe(lista => {this.propostas = lista;
      console.log("lista:> ", lista);
      });
  }

  abrir(item:any){
    if(item){
      this.router.navigate(['/inicio/propostas-de-tcc/alterar-proposta-tcc/'+ (item == undefined ? 'novo': item.id)]);
      return;
    }

    this.router.navigate(['/inicio/propostas-de-tcc/cadastro-tcc/']);
  }
}