import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AuthService } from './services/auth/auth.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProviderGenericoProvider {

  constructor(private http: HttpClient,
              private auth$: AuthService) { }

	get(obj:any) {
		return this.http
				.get(environment.API_URL
				.concat(obj.nomeServico+'/'), 
				this.auth$.httpOptions());		
	}

	getId(obj:any) {
			return this.http
				.get(environment.API_URL
				.concat(obj.nomeServico+'/'+obj.id+'/'), 
				this.auth$.httpOptions());
	}

  post(obj:any){
    
		return this.http
				.post(environment.API_URL
				.concat(obj.nomeServico+'/'), 
			//  .concat('cadastro-tcc/'),
				obj.dados, this.auth$.httpOptions());
  }

  put(obj:any){
	console.log("url>: ", environment.API_URL.concat(obj.nomeServico+'/'+ obj.id +'/'));
	

		return this.http
				.put(environment.API_URL
				.concat(obj.nomeServico+'/'+ obj.id +'/'), 
			//  .concat(`propostas-de-tcc/${id}/`),
				obj.dados, this.auth$.httpOptions());

  }
}